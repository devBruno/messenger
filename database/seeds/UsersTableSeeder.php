<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Bruno Padilla',
        	'email' => 'bruno_padilla@ammmec.com',
        	'password' => bcrypt('123')
        ]);

        User::create([
            'name' => 'Alejandro Castro',
            'email' => 'alejandro_castro@ammmec.com',
            'password' => bcrypt('123')
        ]);

        User::create([
            'name' => 'Ramos',
            'email' => 'ramos@ammmec.com',
            'password' => bcrypt('123')
        ]);
    }
}
