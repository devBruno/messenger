<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/api/conversations', 'ConversationsController@index');

Route::get('/api/messages', 'MessagesController@index');
Route::post('/api/messages', 'MessagesController@store');

